import 'package:flutter/material.dart';
import 'ui/login_page.dart';
import 'routing/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Raleway-Bold',
        primaryColor: Color(0xff01A0C7),
      ),
      home: LoginPage(),
      routes: routes,
    );
  }
}
