import 'dart:io';

class Client{
  int phone_number;
  int id_number;
  File profile_picture;
  String email_address;

  Client(int phone_number, int id_number, String email_address, File profile_picture){
    this.phone_number = phone_number;
    this.id_number = id_number;
    this.email_address = email_address;
    this.profile_picture = profile_picture;
  }
}