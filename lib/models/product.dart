import 'dart:io';
import 'package:flutter/material.dart';

class Product{
  int serial_number;
  String product_name;
  String description;
  List<File> images = List(2);  

  //Constructor
  Product(int serial_number, String product_name, String description, List<File> images){
    this.serial_number = serial_number;
    this.product_name = product_name;
    this.description = description;
    this.images = images;
  }
}