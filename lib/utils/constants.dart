class Constants{
  static final String BASE_URL = 'https://test.insurance-app.innovexsolutions.co.ke/api/';
  static final String LOGIN = 'login/';
  static final String REGISTER = 'register/';
}