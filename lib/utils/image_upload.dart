import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:image_picker/image_picker.dart';

class ImageUpload{

  File image;

  void chooseImageFromGallery() async {
    image = await ImagePicker.pickImage(source: ImageSource.gallery);
  }

  void takeImageFromCamera() async{
    image = await ImagePicker.pickImage(source: ImageSource.camera);
  }
}

