import 'constants.dart';
import 'package:dio/dio.dart';

class NetworkUtil{

  void getHttp() async{
    try{
      Response response = await Dio().get(Constants.BASE_URL);
      print(response.data);
    }catch(error){
      print(error);
    }
  }

  // Future<http.http.Response> login(String phoneNumber, String password) async{
  //     return http.get(Constants.BASE_URL);
  // }
}