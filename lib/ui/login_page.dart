import 'package:flutter/material.dart';
import 'homepage.dart';
import '../utils/network_util.dart';

class LoginPage extends StatefulWidget{
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{

  NetworkUtil _networkUtil = NetworkUtil();

  bool _obscureText = true;



  //Toggle password obscureText property
  void _togglePasswordVisibility(){
    setState(() {
     _obscureText = !_obscureText; 
    });
  }

  @override
  Widget build(BuildContext context){

    //Custom Login Button
    final LoginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 20.0),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => Homepage()));
          _networkUtil.getHttp();
        },
        child: Text(
          'LOGIN',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold
          ),
        ),
      ),
    );

    //Login Page User Interface
    return Scaffold(
      body: SafeArea(
        child: ListView(
            padding: EdgeInsets.all(24.0),
            children: <Widget>[
              SizedBox(height: 20.0,),
              Column(
                children: <Widget>[
                  SizedBox(height: 20.0,),
                  Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 30.0,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30.0,),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Phone Number',
                  prefixIcon: Icon(
                    Icons.phone,
                  )
                ),
                keyboardType: TextInputType.numberWithOptions(),
              ),
              TextFormField(
                obscureText: _obscureText,
                decoration: InputDecoration(
                  labelText: 'Password',
                  prefixIcon: Icon(
                    Icons.lock
                  ),
                  suffixIcon: IconButton(
                    icon: Icon(
                      _obscureText ? Icons.visibility : Icons.visibility_off, 
                      ),
                      onPressed: (){
                        _togglePasswordVisibility();
                      },
                  )
                ),
              ),
              SizedBox(height: 40.0,),
              LoginButton,
              SizedBox(height: 12.0),
              Center(
                child: FlatButton(
                  child: Text("Don't have an account?"),
                  onPressed: (){
                    Navigator.pushNamed(context, '/register');
                  },
                ),
              )
            ],
        )
      ),
    );
  }
}
