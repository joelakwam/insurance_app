import 'package:flutter/material.dart';

class ProfileSection extends StatefulWidget {
  @override
  _ProfileSectionState createState() => _ProfileSectionState();
}

class _ProfileSectionState extends State<ProfileSection> {
  String name = 'John Doe';
  String email = 'john.doe@innovexsolutios.co.ke';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Profile'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            CircleAvatar(
              child: Icon(
                Icons.account_circle,
                color: Colors.white,
                size: 50.0,
              ),
              backgroundColor: Colors.blueAccent,
              radius: 60.0,
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              name,
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Raleway-Bold',
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 5.0,
            ),
            Text(
              email,
              style: TextStyle(fontFamily: 'Raleway-Light'),
            ),
            SizedBox(height: 30.0,),
            Column(
              children: <Widget>[
                Center(
                  child: Column(
                    children: <Widget>[
                    ListTile(
                      contentPadding: EdgeInsets.fromLTRB(30.0, 1.0, 0.0, 1.0),
                      leading: Icon(Icons.phone),
                      title: Text('0712345678'),
                      subtitle: Text('Phone Number'),
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.fromLTRB(30.0, 0.0, 0.0, 1.0),
                      leading: Icon(Icons.location_on),
                      title: Text('Nairobi, Langata, Otiende'),
                      subtitle: Text('Location'),
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.fromLTRB(30.0, 0.0, 0.0, 1.0),
                      leading: Icon(Icons.account_box),
                      title: Text('36258565'),
                      subtitle: Text("ID Number"),
                    ),
                  ],
                ))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
