import 'package:flutter/material.dart';
import 'dart:io';
import '../utils/image_upload.dart';
import 'map.dart';

class SignUpPage extends StatefulWidget {
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  static ImageUpload _imageUpload = ImageUpload();
  var _formKey = GlobalKey<FormState>();

  int currentStep = 0;
  bool complete = false;
  static bool obscureText = true;

  void _togglePasswordVisibility(){
    setState(() {
     obscureText = !obscureText; 
    });
  }

  //Method for jumping to the next Step
  void nextStep() {
    if(_formKey.currentState.validate()){
      (currentStep + 1) != steps.length
        ? goTo(currentStep + 1)
        : setState(() {
            complete = true;
            Navigator.push(context, MaterialPageRoute(builder: (context) => Map()));
          });
    }
  }

  //For jumping to a tapped Step
  void goTo(int step) {
    setState(() {
      currentStep = step;
    });
  }

  //For jumping a Step back
  void previousStep() {
    if (currentStep > 0) {
      goTo(currentStep - 1);
    }
  }



  static void _showDialog(BuildContext context){
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text('Choose Image'),
          actions: <Widget>[
            FlatButton(
              child: Text('Take picture using camera'),
              onPressed: (){
                _imageUpload.takeImageFromCamera();
              },
            ),
            FlatButton(
              child: Text('Select from gallery'),
              onPressed: (){
                _imageUpload.chooseImageFromGallery();
              },
            ),
          ],
        );
      }
    );
  }

  //List of Steps in the Stepper
  static List<Step> steps = [
    Step(
      title: Text('New Account'),
      isActive: true,
      state: StepState.complete,
      content: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
              labelText: 'First Name',
              prefixIcon: Icon(Icons.account_circle)
              ),
            validator: (value) => value.isEmpty ? 'First Name cannot be empty' : null,
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Last Name',
              prefixIcon: Icon(Icons.account_circle)
              ),
            validator: (value) => value.isEmpty ? 'Last Name cannot be empty' : null,
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Email Address (Optional)',
              prefixIcon: Icon(Icons.email)
              ),
            
            validator: (value) => !value.contains('@')? 'Enter valid email' : null,
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'ID Number',
              prefixIcon: Icon(Icons.account_box)
              ),
            validator: (value) => value.isEmpty ? 'ID Number cannot be empty' : null,
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Phone Number',
              prefixIcon: Icon(Icons.phone)
              ),
            validator: (value) => value.isEmpty ? 'Enter a valid phone number' : null,
            keyboardType: TextInputType.numberWithOptions(),
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Password',
              prefixIcon: Icon(Icons.lock),
              suffixIcon: IconButton(
                icon: Icon(obscureText ? Icons.visibility : Icons.visibility_off),
                onPressed: (){
                  // _togglePasswordVisibility;
                },
                ),
              ),
            validator: (value) => value.length < 6 ? 'Password must be at least 6 characters' : null,
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Re-enter password',
              prefixIcon: Icon(Icons.lock)
              ),
          ),
        ],
      ),
    ),
    Step(
      title: Text('Profile Picture'),
      state: StepState.editing,
      content: Column(
        children: <Widget>[
          Center(
              child: Container(
            child: Column(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.add_a_photo),
                  color: Colors.blueGrey,
                  iconSize: 50.0,
                  onPressed:(){},
                ),
                Text('Upload photo')
              ],
            ),
          ))
        ],
      ),
    ),
    Step(
      title: Text('Address'),
      state: StepState.editing,
      content: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Country, City, Street',
              prefixIcon: Icon(Icons.location_on),
            ),
          validator: (value) => value.isEmpty ? 'Please enter your locatiom' : null,
          ),
        ],
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create an account'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Form(
              key: _formKey,
              child: Stepper(
                steps: steps,
                currentStep: currentStep,
                onStepContinue: () {
                  nextStep();
                },
                onStepCancel: previousStep,
                onStepTapped: (step) => goTo(step),
              ),
            ),
          )
        ],
      ),
    );
  }
}
