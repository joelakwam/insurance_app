import 'package:flutter/material.dart';

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: Center(
        child: Column(
          children: <Widget>[
            DropdownButton(
              items: [
                DropdownMenuItem(
                  child: Text('TV'),
                  value: 'TV',
                ),
                DropdownMenuItem(
                  child: Text('Fridge'),
                  value: 'fridge',
                ),
                DropdownMenuItem(
                  child: Text('Laptop'),
                  value: 'laptop',
                ),
                DropdownMenuItem(
                  child: Text('Microwave'),
                  value: 'microwave',
                ),
                DropdownMenuItem(
                  child: Text('Washing Machine'),
                  value: 'washing machine',
                ),
                DropdownMenuItem(
                  child: Text('Radio'),
                  value: 'radio',
                ),
              ],
            ),
            TextFormField(
                decoration: InputDecoration(
                labelText: 'Brand/Company of your product',
              )
            ),
            TextFormField(
                decoration: InputDecoration(
                labelText: 'Serial Number',
              )
            ),
            Column(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.add_a_photo),
                  onPressed: (){},
                )
              ],
            ),
            RaisedButton(
              child: Icon(Icons.add),
              color: Color(0xff01A0C7),
              onPressed: (){},
            )
          ],
        ),
      ),
    );
  }
}
