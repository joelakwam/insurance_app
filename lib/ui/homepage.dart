import 'package:flutter/material.dart';
import 'package:insurance_app/ui/add_product.dart';
import 'package:insurance_app/ui/claim_section.dart';
import 'package:insurance_app/ui/profile_section.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  int _currentIndex = 0;

  void _incrementTab(index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xff01A0C7),
      ),
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              'Insurance App',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            bottom: TabBar(
              indicatorColor: Colors.white,
              tabs: <Widget>[
                Tab(
                  icon: Icon(Icons.list),
                  text: 'Products',
                ),
                Tab(
                  icon: Icon(Icons.add_circle),
                  text: 'Add Cover',
                ),
                Tab(
                  icon: Icon(Icons.folder_open),
                  text: 'Claim',
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              Icon(Icons.directions_bike),
              AddProduct(),
              ClaimSection(),
            ],
          ),
          drawer: Drawer(
            child: ListView(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text("John Doe"),
                  accountEmail: Text("john.doe@gmail.com"),
                  currentAccountPicture: CircleAvatar(
                    backgroundColor:
                        Theme.of(context).platform == TargetPlatform.iOS
                            ? Colors.blue
                            : Colors.white,
                    child: IconButton(
                      icon: Icon(Icons.account_circle),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProfileSection()));
                      },
                    ),
                  ),
                ),
                ListTile(
                  leading: Icon(
                    Icons.settings,
                    color: Colors.black,
                  ),
                  title: Text('Settings'),
                ),
                
                ListTile(
                  leading: Icon(
                    Icons.exit_to_app,
                    color: Colors.black,
                  ),
                  title: Text('Logout'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
