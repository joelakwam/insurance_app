import 'package:flutter/material.dart';
import '../ui/add_product.dart';
import '../ui/claim_section.dart';
import '../ui/homepage.dart';
import '../ui/map.dart';
import '../ui/profile_section.dart';
import '../ui/signup_page.dart';

final routes = {
  '/register'     : (BuildContext context) => SignUpPage(),
  '/map'          : (BuildContext context) => Map(),
  '/homepage'     : (BuildContext context) => Homepage(),
  '/add-product'  : (BuildContext context) => AddProduct(),
  '/profile'      : (BuildContext context) => ProfileSection(),
  '/claim'        : (BuildContext context) => ClaimSection(),
};
